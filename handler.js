'use strict';

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://ip-172-31-29-17.ec2.internal:27017/";

exports.getUserDevicesInfo = (event, context, callback) => {
    var userId = event.userId;

    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var selectedDb = db.db("home-controller-db");
        
        selectedDb.collection("deviceInfo").find({"userId":userId}).toArray(function(err, result){
            if (err) throw err;
            
            if(result){
              db.close();
              callback(null, result);
            }else{
              db.close();
              callback(null, "User not recognized!");
            }
        });
      });
}